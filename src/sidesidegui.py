###############################
##
##  sidesidegui v prealpha
##
###############################

## Libraries and Imports
import sys
import os.path
import functools
#import socket
import time
import getpass
import telnetlib
from datetime import datetime
from Tkinter import *
import atexit
from email.mime import *
from email.mime.image import MIMEImage
from email.mime.audio import MIMEAudio
from email.mime.message import MIMEMessage
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from struct import pack
from cx_Freeze import setup, Executable
build_exe_options   = {"includes": ["Tkinter"]}

# Window Information for tkinter
title               = "Side to Side Gui Assistant"
version             = "0.2.2"

## Initialize Variables
master              = Tk()
master.wm_title(title+" v"+version)

vars        =   list()
currvals    =   list()
lessbtn     =   list()
scales      =   list()
morebtn     =   list()
minvals     =   list()
stpsz       =   list()
maxvals     =   list()

connected   =   0
buffer      =   1024

Nscales     =   20
    
pltfrm      =   IntVar()
pltfrm.set(2)

acc         =   list()
dec         =   list()
ff          =   list()
pthsrc      =   list()
pthdst      =   list()

acc.append("x_acce")
dec.append("x_dece")
ff.append("xxx.ff")
pthsrc.append(1)
pthdst.append(8)

acc.append("cnd.acc")
dec.append("cnd.dec")
ff.append("nt22.ff")
pthsrc.append(2)
pthdst.append(7)

s           =   telnetlib.Telnet()

## Functions
def updValue(event, param):
    currvals[param].delete(0,END)
    currvals[param].insert(5,scales[param].get())
    x = s.get_socket()
    if (x):
        print(vars[param].get()+" = "+currvals[param].get())
        s.get_socket().send(str(vars[param].get())+" = "+str(currvals[param].get())+"\r\n")
    else:
        print("Not connected, Unable to Send: '"+vars[param].get()+" = "+currvals[param].get()+"'")
def updSlider(event, param):
    if (vars[param]!=""):
        scales[param].configure(from_=float(minvals[param].get()), to=float(maxvals[param].get()), tickinterval=0)
        scales[param].set(float(currvals[param].get()))
def decrement(param):
    updSlider(0, param)
    scales[param].set(float(scales[param].get())-float(stpsz[param].get()))
    updValue(0, param)
    lessbtn[param].focus_set()
def increment(param):
    updSlider(0, param)
    scales[param].set(float(scales[param].get())+float(stpsz[param].get()))
    updValue(0, param)
    morebtn[param].focus_set()
def updvar(event, param):
    pass
def deleteRow(prm):
    vars[prm].delete(0,END)
    minvals[prm].delete(0,END)
    stpsz[prm].delete(0,END)
    maxvals[prm].delete(0,END)
    currvals[prm].delete(0,END)
def updRow(prm,var,cur,minval,stpsiz,maxval):
    vars[prm].insert(10,var)
    currvals[prm].insert(10,cur)
    minvals[prm].insert(10,minval)
    stpsz[prm].insert(10,stpsiz)
    maxvals[prm].insert(10,maxval)
    scales[prm].configure(from_=float(minvals[prm].get()), to=float(maxvals[prm].get()), tickinterval=0)
    scales[prm].set(float(currvals[prm].get()))
def setPltfrm(event, param):
    if (param==-1):
        param=pltfrm.get()
    if (param==0 or param==1):
        for i in range(Nscales*2):
            deleteRow(i)
        for i in range((pthdst[param]-pthsrc[param]+1)/2):
            updRow(i,acc[param]+"["+str(pathnum.get())+","+str(i+1)+"]","30","0","2","100")
        for j in range((pthdst[param]-pthsrc[param]+1)/2):
            updRow(j+(i+1),dec[param]+"["+str(pathnum.get())+","+str(j+1)+"]","30","0","2","100")
        for k in range(Nscales-(i+j+2)):
            updRow(k+(i+j+2),ff[param]+"["+str(pathnum.get())+",1,"+str(k+1)+"]","0","-0.1","0.001","0.1")
        for i in range((pthdst[param]-pthsrc[param]+1)/2):
            updRow(i+Nscales,acc[param]+"["+str(pathnum.get())+","+str(i+pthsrc[param]+(pthdst[param]-pthsrc[param]+1)/2)+"]","30","0","2","100")
        for j in range((pthdst[param]-pthsrc[param]+1)/2):
            updRow(j+(i+1)+Nscales,dec[param]+"["+str(pathnum.get())+","+str(j+pthsrc[param]+(pthdst[param]-pthsrc[param]+1)/2)+"]","30","0","2","100")
        for k in range(Nscales-(i+j+2)):
            updRow(k+(i+j+2)+Nscales,ff[param]+"["+str(pathnum.get())+",2,"+str(k+1)+"]","0","-0.1","0.001","0.1")
    else:
        return

def connect():
    global s
    def set_option(socket, command, option):
        socket.send("%s%s\x18" % (telnetlib.IAC, telnetlib.WILL))
        time.sleep(0.1)
        socket.send("%s%s\x18\x00%s%s" %(telnetlib.IAC,telnetlib.SB,telnetlib.IAC,telnetlib.SE))
        time.sleep(0.1)
        socket.send("\r\n")
    try:
        s = telnetlib.Telnet(ipentry.get(),23,1)
    except:
        print("The connection attempt was unsuccessful to '"+ ipentry.get()+"'")
        return
    s.set_option_negotiation_callback(set_option)
    time.sleep(0.1)
    x = s.read_eager()
    s.get_socket().sendall("as\r\n")
    x = s.read_until("Incorrect Username",0.1)
    constat.select()
    print("Connected to '"+ ipentry.get()+"'")
def disconnect():
    s.close()
    constat.deselect()
    print("Disconnected")

## Connection Group
group1 = LabelFrame(master)
group1.grid(row=0,column=0)
Label(group1,text="IP", font=("Helvetica",10)).grid(row=1,column=0,columnspan=2)
con = Button(group1,text="Connect to KCWin",command=connect, font=("Helvetica",10), width=16).grid(row=2,column=0,columnspan=3)
dcon = Button(group1,text="Disconnect",command=disconnect, font=("Helvetica",10), width=20).grid(row=3,column=0,columnspan=4)
ipentry = Entry(group1, font=("Helvetica",10))
ipentry.grid(row=1,column=2,columnspan=2)
ipentry.insert(5,"192.9.200.70")
constat = Checkbutton(group1, text="", var=connected, state=DISABLED)
constat.grid(row=2,column=3,columnspan=1)
constat.deselect()

## Platform and Path Group
group2 = LabelFrame(master)
group2.grid(row=0,column=1)
rbp1 = Radiobutton(group2, font=("Helvetica",10),text="Legacy Amat",var=pltfrm,value=0,command=functools.partial(setPltfrm,event=0, param=0)).grid(row=0,sticky=W)
rbp2 = Radiobutton(group2, font=("Helvetica",10),text="Common Code",var=pltfrm,value=1,command=functools.partial(setPltfrm,event=0, param=1)).grid(row=1,sticky=W)
rbp3 = Radiobutton(group2, font=("Helvetica",10),text="Other",var=pltfrm,value=2,command=functools.partial(setPltfrm,event=0, param=2)).grid(row=2,sticky=W)
Label(group2,text="Path = ", font=("Helvetica",10)).grid(row=0,column=1,rowspan=2,sticky=E)
pathnum = Entry(group2, font=("Helvetica",10),width=5)
pathnum.bind("<FocusOut>", functools.partial(setPltfrm, param=-1))
pathnum.bind("<Return>", functools.partial(setPltfrm, param=-1))
pathnum.grid(row=0,column=2,rowspan=2,sticky=W)
pathnum.insert(5,0)

## Variables and Scales Group
group3 = LabelFrame(master)
group3.grid(row=1,columnspan=2)
for i in range(Nscales*2):
    scgroup = LabelFrame(group3, bd=1)
    if (i<Nscales):
        scgroup.grid(row=(i%Nscales),column=0,columnspan=4)
    else:
        scgroup.grid(row=(i%Nscales),column=5,columnspan=4)
        
    if (i==0 or i==Nscales):
        Label(scgroup,text="varName", font=("Helvetica",10)).grid(row=1,column=0)
        Label(scgroup,text="Value", font=("Helvetica",10)).grid(row=1,column=1)
        Label(scgroup,text="").grid(row=1,column=2)
        Label(scgroup,text="Scale", font=("Helvetica",10)).grid(row=1,column=3)
        Label(scgroup,text="").grid(row=1,column=4)
        Label(scgroup,text="Min", font=("Helvetica",10)).grid(row=1,column=5)
        Label(scgroup,text="Step", font=("Helvetica",10)).grid(row=1,column=6)
        Label(scgroup,text="Max", font=("Helvetica",10)).grid(row=1,column=7)
        
    ev = Entry(scgroup, font=("Helvetica",10),width=13)
    ec = Entry(scgroup, font=("Helvetica",10),width=7)
    lb = Button(scgroup, font=("Helvetica",10),text=" - ", command=functools.partial(decrement, param=int(i)))
    sc = Scale(scgroup, from_=0, to=200, length=100, tickinterval=0, orient=HORIZONTAL, resolution=0.0001, showvalue=0)
    mb = Button(scgroup, font=("Helvetica",10),text=" + ", command=functools.partial(increment, param=int(i)))
    emin = Entry(scgroup, font=("Helvetica",10),width=6)
    estp = Entry(scgroup, font=("Helvetica",10),width=6)
    emax = Entry(scgroup, font=("Helvetica",10),width=6)
    
    ev.bind("<FocusOut>", functools.partial(updvar, param=int(i)))
    ev.bind("<Return>", functools.partial(updvar, param=int(i)))
    ec.bind("<FocusOut>", functools.partial(updSlider, param=int(i)))
    ec.bind("<Return>", functools.partial(updSlider, param=int(i)))
    sc.bind("<Button-1>", functools.partial(updSlider, param=int(i)), functools.partial(updvar, param=int(i)))
    sc.bind("<ButtonRelease-1>", functools.partial(updValue, param=int(i)))
    emin.bind("<FocusOut>", functools.partial(updSlider, param=int(i)))
    emin.bind("<Return>", functools.partial(updSlider, param=int(i)))
    estp.bind("<FocusOut>", functools.partial(updSlider, param=int(i)))
    estp.bind("<Return>", functools.partial(updSlider, param=int(i)))
    emax.bind("<FocusOut>", functools.partial(updSlider, param=int(i)))
    emax.bind("<Return>", functools.partial(updSlider, param=int(i)))
    
    ev.grid(row=2, column=0)
    ec.grid(row=2, column=1)
    lb.grid(row=2,column=2)
    mb.grid(row=2,column=4)
    sc.grid(row=2,column=3)
    emin.grid(row=2, column=5)
    estp.grid(row=2, column=6)
    emax.grid(row=2, column=7)

    ev.insert(5,"var"+str(i+1))
    ec.insert(5,0)
    sc.set(0)
    emin.insert(5,0)
    estp.insert(5,1)
    emax.insert(5,200)
    
    vars.append(ev)
    currvals.append(ec)
    lessbtn.append(lb)
    scales.append(sc)
    morebtn.append(mb)
    minvals.append(emin)
    stpsz.append(estp)
    maxvals.append(emax)
mainloop( )

