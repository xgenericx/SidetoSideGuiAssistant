--------------------------------------------------------
Side to Side Gui Assistant
Version 0.2.2
--------------------------------------------------------

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://+ooooooo+::::::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::/+osyhhhhhhhhhhhhh/::::
:::::::::::::::::::::::::::::::::::::::::::::::::::::://+++ooossssso/:/+osyhhhhhhhhhhhhhhhhdddm/::::
:::::::::::::::::::::::::::::::////:::::///++ooossyhhhhhhhhhhhhhhhhddhhhhhhhhhhhhhhhdddddmmmmmm/::::
::::::::::::::::::::::::/+osyhhhhhhhyhhhhhhhhhhhhhhhdhhdddddddddmmmmmdddddhhhddddddmmmmmmmmmmmd/::::
::::::::::::::::::::osyhhhhhhdddddddddddddddddddddddmmmmmmmmmmmmmmmmdhhhddddmmmmmmmmmmmmddhyso/:::::
---------:://++osssydhhddddhyymmmmmmmmmmmmmmmmmmmmmdyyssoo+/mmmmmmmdddmmmmmmmmmmmmdhyyysso+///:-----
://+ossyyhysssyhhhhhhys++/::::yhhhhhhhhhyyysso++//:::-------mmmmmmmmmmmmmmmmdhys+/:::+sso++///:-----
:+++/:::::::/oshhys+::---------:::::::::--------------------ydmmmmmdddhyso+:::--::/+ossso++///:-----
---------/+shys+::-------------------------------------------::::::::::---:/+osyhhhhhysso++///:o+:--
---------/+::------------------------------------------------------::/+osyhhhys+/--/oyyysoooooshhs:-
-----------------------------------------------------------------oyhhhhhhhh+-`    `.:+shhhhhhhddddy-
-----------------------------------------------------------------/ydmmdddhhhhyo/osyhhhhdddddddmmmms-
-------------------------------------------------------------------/ddmmmmddddhhddddddddddddmmmmmms-
--------------------------------------------------------------------ommmddmmmddddmmmmdddmmmmmmmmmms-
---------------------------------------------------------------------ommmmmmmdhdmmmmmmmmmmmmmmmmmms-
.....................................................................-ymmmmmmmmmmmmmmddddddmmmmmmms.
......................................................................ymmmmmmmmmmmddddddmmmmmmmmmms.
......................................................................ymmmmmmmmmmmmmmmmmmmmmmmmmmms.
......................................................................ymmmmmmmmmmmmmmmmmmmmmmmmmmms.
......................................................................ymmmmmmmmmmmmmmmmmmmmmmmmmmms.
......................................................................ymmmmmmmmmmmmmmmmmmmmmmmmmmms.
......................................................................smmmmmmmmmmmmmmmmmmmmmmmmmmms.
......................................................................smmmmmmmmmmmmmmmmmmmmmmmmmmms.
......................................................................smmmmmmmmmmmmmmmmmmmmmmmmmmms.
......................................................................smmmmmmmmmmmmmmmmmhyhydmmmmms.
``````````````````````````````````````````````````````````````````````smmmmmmmmmmmmmmmmdsyo++dmmmms`
``````````````````````````````````````````````````````````````````````smmmmmmmmmmmmmmmmoyyyyodmmmms`
`````````````````````````````````````````````````````````````````.//:/ymmmmmmmmmmmmmmmmdyyyyodmmmms`
`````````````````````````````````````````````````````````````````:ssyyhhddmmmmmmmmmmmmmmdhhdydmmmms`
`````````````````````````````````````````````````````````````````:sooosddddmmmddmmmmmmmmmmmNMmmmmms`
`````````````````````````````````````````````````````````````````:oooosmmddhsyhhhdmmmmmmmmmmMmmmmmo`
`````````````````````````````````````````````````````````````````.://+smmmmhooosydmmmmmmmmddmmys+/-`
``````````````````````````````````````````````````````````````````````.-/shhooosymmdddddddddh+``````
```````````````````````````````````````````````````````````````````````````.:/ooymddddys+/-.````````
```````````````````````````````````````````````````````````````````````````````:+o/:.```````````````

You too can make quick changes to tuning parameters with instantaneous feedback!

1.  Make sure your jig/controller/computer are all hooked up for waveform measurement!

2.  Open SSGA v0.1 and connect to the IP of your dreams/desires!

3.  ???!

4a. Profit!

4b.     Clicking "Legacy AMAT" or "Common Code" will set the variables and scales to those
platform defaults!
    Legacy AMAT: x_acce[], x_dece[], and xxx.ff[]                                                   !
    Common Code: cnd.acc[], cnd.dec[], and nt22.ff[]                                                !
    
4c.     Enter a path name OR path number in the 'path = ' entry in order to set the
motion path variables for that path name/number.

4c.     Set the minimum/maximum adjustable variable range, and the step size!

4d.     Clicking '-' or '+' will decrement or increment, respectively, the variable value
by the specified step size! Once clicked, you can use the SPACE bar to activate that
button again repeatedly without the mouse being in position!

4e.     Slide the scale using your mouse to set the scale directly!

4*.     The scale takes priority over the value in the text box!

4**.    Whatever variable is in the variable name box is whats going to be sent.